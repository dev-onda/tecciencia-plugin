 _dir = File.dirname(__FILE__)
class TeccienciaPluginRelacionamentoController < MyProfileController
  def new_community
    @user = user
    tecciencia = Community['tecciencia']
    unless tecciencia.members.include? user
      throw 'Você deve ser membro do tecciencia para criar uma subcomunidade'
    end
    @community = Community.new params[:community]
    @community.environment = environment
    @community.theme = tecciencia.theme
    if request.post? && @community.valid?
      @community.save!
      @community.add_admin user
      #@community.apply_template Community['grupo-tecciencia']
      SubOrganizationsPlugin::Relation.add_children(tecciencia, @community)
      redirect_to  @community.url
    end
  end
end

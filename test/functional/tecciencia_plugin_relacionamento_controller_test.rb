require 'test_helper'

class TeccienciaPluginRelacionamentoControllerTest < ActionController::TestCase

  def setup
    @controller = TeccienciaPluginRelacionamentoController.new
    @profile = create_user('testuser').person
    login_as('testuser')
    @someone = create_user('someone').person
    @tecciencia = Community.create name:'tecciencia'
    @tecciencia.theme = 'meu-tema'
    @tecciencia.add_member @profile
    @tecciencia.save!
  end

  attr_reader :profile, :tecciencia, :someone

  should 'comunidade tem o tema do tecciencia' do
    assert_difference 'Community.count' do
      post :new_community, profile:profile.identifier, :community => {name:'Meu teste', description:'Comunidade criada para um teste bacana'}
      assert_response :redirect
      assert Community['meu-teste']
      assert_equal 'meu-tema', Community['meu-teste'].theme
    end
  end

  should 'usuario nao eh um membro' do
    assert_no_difference 'Community.count' do
      login_as('someone')
      assert_raise ArgumentError do
        post :new_community, profile:someone.identifier, :community => {name:'Meu teste', description:'Comunidade criada para um teste bacana'}
      end
      assert_equal Community['teste'], nil
    end
  end

  should 'Comunidade eh uma filha de Tecciencia' do
    assert_difference 'Community.count' do
      post :new_community, profile:profile.identifier, :community => {name:'Meu teste', description:'Comunidade criada para um teste bacana'}
      assert_response :redirect
      assert Community['meu-teste']
      children = Community.children(Community['tecciencia'])
      assert_equal children.include?(Community['meu-teste']), true
    end
  end

end
